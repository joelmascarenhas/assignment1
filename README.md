# README #
AI Assignment repo

### What is this repository for? ###

* This is a common repo for the CSE 571 - Artificial Intelligence Assignment # 1
* The code has logics for Basic DFS, BFS, UCS, A* , Basic Heuristic Design for two problems as to reach corners and to eat all food, State Representation for a problem.

### How do I get set up? ###

You can just refer the PDF in each assignments for running the code